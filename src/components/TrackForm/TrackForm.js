import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import FileInput from "../UI/Form/FileInput";

const TrackForm = ({onSubmit, albums, loading, error}) => {
    const [state, setState] = useState({
        title: '',
        duration: '',
        album: '',
        trackNumber: '',
        image: ''
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    return (
        <form onSubmit={submitFormHandler} noValidate>
            <Grid container direction="column" spacing={2}>
                <FormElement
                    required
                    select
                    label="Album"
                    name="album"
                    value={state.album}
                    onChange={inputChangeHandler}
                    options={albums}
                    error={getFieldError('album')}
                />

                <FormElement
                    required
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />

                <FormElement
                    required
                    label="Duration"
                    type="number"
                    name="duration"
                    value={state.duration}
                    onChange={inputChangeHandler}
                    error={getFieldError('duration')}
                />

                <FormElement
                    required
                    label="Track Number"
                    type="number"
                    name="trackNumber"
                    value={state.trackNumber}
                    onChange={inputChangeHandler}
                    error={getFieldError('trackNumber')}
                />

                <Grid item xs>
                    <FileInput
                        name="image"
                        label="Image"
                        onChange={fileChangeHandler}
                        error={getFieldError('image')}
                    />
                </Grid>

                <Grid item xs>
                    <ButtonWithProgress
                        type="submit" color="primary" variant="contained"
                        loading={loading} disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </form>
    );
};

export default TrackForm;