import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import {Avatar, Menu, MenuItem} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import Grid from "@material-ui/core/Grid";

import unnamedAvatar from '../../../../assets/images/unnamed.png';

const UserMenu = ({user, avatarImage}) => {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    if (avatarImage) {
        avatarImage = avatarImage
    }

    if (!avatarImage) {
        avatarImage = unnamedAvatar
    }

    return (
        <Grid item container alignItems="center">
            <Grid item xs>
                <Avatar alt="User avatar" src={avatarImage}/>
            </Grid>
            <Grid item xs>
                <Button onClick={handleClick} color="inherit">Hello, {user.displayName}!</Button>
                <Menu
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem component={Link} to='/track_history'>Track History</MenuItem>
                    <MenuItem component={Link} to='/albums-new'>Add new album</MenuItem>
                    <MenuItem component={Link} to='/tracks-new'>Add new track</MenuItem>
                    <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
                </Menu>
            </Grid>
        </Grid>
    );
};

export default UserMenu;