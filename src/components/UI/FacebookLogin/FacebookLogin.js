import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import Button from "@material-ui/core/Button";
import FacebookIcon from '@material-ui/icons/Facebook';
import {facebookLogin} from "../../../store/actions/usersActions";
import {useDispatch} from "react-redux";

const FacebookLogin = () => {
    const dispatch = useDispatch();

    const facebookResponse = response => {
        if (response.id) {
            dispatch(facebookLogin(response));
            console.log(response)
        }
    }

    return (
        <FacebookLoginButton
            appId='529915631366097'
            fields='name,email,picture'
            render={props => (
                <Button
                    fullWidth
                    color="primary"
                    variant="outlined"
                    startIcon={<FacebookIcon/>}
                    onClick={props.onClick}
                >
                    Login with Facebook
                </Button>
            )}
            callback={facebookResponse}
        />
    );
}

export default FacebookLogin;