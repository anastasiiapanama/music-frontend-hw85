import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FileInput from "../UI/Form/FileInput";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const AlbumForm = ({onSubmit, artists, loading, error}) => {
    const [state, setState] = useState({
        artist: '',
        title: '',
        year: '',
        image: '',
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form onSubmit={submitFormHandler} noValidate>
            <Grid container direction="column" spacing={2}>

                <FormElement
                    required
                    select
                    label="Artist"
                    name="artist"
                    value={state.artist}
                    onChange={inputChangeHandler}
                    options={artists}
                    error={getFieldError('artist')}
                />

                <FormElement
                    required
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />

                <FormElement
                    required
                    label="Year"
                    type="number"
                    name="year"
                    value={state.year}
                    onChange={inputChangeHandler}
                    error={getFieldError('year')}
                />

                <Grid item xs>
                    <FileInput
                        name="image"
                        label="Image"
                        onChange={fileChangeHandler}
                        error={getFieldError('image')}
                    />
                </Grid>

                <Grid item xs>
                    <ButtonWithProgress
                        type="submit" color="primary" variant="contained"
                        loading={loading} disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </form>
    );
};

export default AlbumForm;