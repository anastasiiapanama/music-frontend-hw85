import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {historyList} from "../../store/actions/trackHistoryActions";

import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Card, CardActions, CardContent, CardHeader, IconButton} from "@material-ui/core";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    },
    card: {
        height: '100%',
        width: '70%',
        display: "flex",
        justifyContent: 'space-between'
    },
    content: {
        display: 'flex',
        alignItems: 'center'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
}));

const TrackHistory = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.trackHistory.tracks);
    const userId = useSelector(state => state.users.user._id);

    useEffect(() => {
        dispatch(historyList(userId));
    }, [dispatch, userId]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">History</Typography>
                </Grid>
            </Grid>
            <Grid item container direction="column" spacing={1}>
                {tracks && tracks.map(track => {
                    return (
                        <Grid item key={track._id} id={track._id}>
                            <Card className={classes.card}>
                                <CardHeader title={track.track.trackNumber}/>
                                <CardContent className={classes.content}>
                                    <strong style={{marginLeft: '10px'}}>
                                        {track.track.title}
                                    </strong>
                                    <p style={{paddingLeft: '100px'}}>Listened to: {track.datetime}</p>
                                </CardContent>
                                <CardActions>
                                    <IconButton color="primary">
                                        <PlayCircleOutlineIcon/>
                                    </IconButton>
                                </CardActions>
                            </Card>
                        </Grid>
                    )
                })}
            </Grid>
        </Grid>
    );
};

export default TrackHistory;