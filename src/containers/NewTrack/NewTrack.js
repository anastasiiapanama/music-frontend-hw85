import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {fetchAlbumsList} from "../../store/actions/albumsActions";
import TrackForm from "../../components/TrackForm/TrackForm";
import {createTrack} from "../../store/actions/tracksActions";

const NewTrack = () => {
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    const error = useSelector(state => state.tracks.createTrackError);
    const loading = useSelector(state => state.tracks.createTrackLoading);

    useEffect(() => {
        dispatch(fetchAlbumsList());
    }, [dispatch]);

    const onTrackFormSubmit = async trackData => {
        dispatch(createTrack(trackData));
        console.log(trackData)
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">Add new track</Typography>
            </Grid>
            <Grid item xs>
                <TrackForm
                    onSubmit={onTrackFormSubmit}
                    albums={albums}
                    loading={loading}
                    error={error}
                />
            </Grid>
        </Grid>
    );
};

export default NewTrack;