import React from 'react';
import {Card, CardActionArea, CardActions, CardContent, CardMedia, Icon, IconButton} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import DeleteIcon from "@material-ui/icons/Delete";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useSelector} from "react-redux";
import CloudDoneIcon from "@material-ui/icons/CloudDone";
import CloudOffIcon from "@material-ui/icons/CloudOff";
import {apiURL} from "../../config";
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 250,
    },
});

const AlbumItem = ({image, title, year, id, published, onDeleteAlbumButton, publishButton}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    if (published) {
        published = <Icon>
            <CloudDoneIcon/>
        </Icon>
    } else {
        published = <IconButton onClick={publishButton}>
            <CloudOffIcon/>
        </IconButton>
    }

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.root}>
                <CardActionArea>
                    <CardMedia
                        image={cardImage}
                        title={title}
                        className={classes.media}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            Year: {year}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <IconButton component={Link} to={'/tracks/' + id}>
                        <ArrowForwardIcon/>
                    </IconButton>
                    {user?.role === 'admin' && (
                        <IconButton onClick={onDeleteAlbumButton}>
                            <DeleteIcon/>
                        </IconButton>
                    )}
                    {published}
                </CardActions>
            </Card>
        </Grid>
    );
};

export default AlbumItem;