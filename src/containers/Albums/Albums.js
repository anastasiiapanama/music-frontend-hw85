import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum, fetchAlbums, publishAlbum} from "../../store/actions/albumsActions";

import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import AlbumItem from "./AlbumItem";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import {useParams} from "react-router-dom";
import {logDOM} from "@testing-library/react";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Albums = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const albums = useSelector(state => state.albums.albums);
    const loading = useSelector(state => state.albums.albumsLoading);

    useEffect(() => {
        dispatch(fetchAlbums(match.params.id));
    }, [dispatch, match.params.id]);

    const publishAlbumButton = (id) => {
        dispatch(publishAlbum(id));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Albums list</Typography>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {user?.role === 'admin' && (
                    <>
                        {loading ? (
                            <Grid container justify="center" alignItems="center" className={classes.progress}>
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : albums.map(album => {
                            return (
                                <AlbumItem
                                    key={album._id} id={album._id}
                                    image={album.image}
                                    title={album.title}
                                    year={album.year}
                                    onDeleteAlbumButton={() => dispatch(deleteAlbum(album._id))}
                                    published={album.published}
                                    publishButton={() => publishAlbumButton(album._id)}
                                />
                            )
                        })}
                    </>
                )}
                {user?.role === 'user' && (
                    <>
                        {loading ? (
                            <Grid container justify="center" alignItems="center" className={classes.progress}>
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : albums.map(album => (
                            <React.StrictMode key={album._id}>
                                {album.published ? (
                                    <AlbumItem
                                        key={album._id} id={album._id}
                                        image={album.image}
                                        title={album.title}
                                        year={album.year}
                                        onDeleteAlbumButton={() => dispatch(deleteAlbum(album._id))}
                                        published={album.published}
                                    />
                                ) : (
                                    <Grid item>
                                        <Typography>No publish albums yet</Typography>
                                    </Grid>
                                )}
                            </React.StrictMode>
                        ))}
                    </>
                )}
                {user?.role === undefined && (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <Typography>Sign in to watch albums</Typography>
                        </Grid>
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
}

export default Albums;