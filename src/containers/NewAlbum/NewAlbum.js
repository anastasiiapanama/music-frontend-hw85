import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {fetchArtists} from "../../store/actions/artistsActions";
import AlbumForm from "../../components/AlbumForm/AlbumForm";
import {createAlbum} from "../../store/actions/albumsActions";

const NewAlbum = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const error = useSelector(state => state.albums.createAlbumError);
    const loading = useSelector(state => state.albums.createAlbumLoading);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    const onProductFormSubmit = async albumData => {
        dispatch(createAlbum(albumData));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">New album</Typography>
            </Grid>
            <Grid item xs>
                <AlbumForm
                    onSubmit={onProductFormSubmit}
                    artists={artists}
                    loading={loading}
                    error={error}
                />
            </Grid>
        </Grid>
    );
};

export default NewAlbum;