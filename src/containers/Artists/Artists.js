import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";

import ArtistItem from "./ArtistItem";
import {deleteArtist, fetchArtists, publishArtist} from "../../store/actions/artistsActions";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Artists = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const artists = useSelector(state => state.artists.artists);
    const loading = useSelector(state => state.artists.artistsLoading);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    const onDeleteHandler = id => {
        dispatch(deleteArtist(id));
    };

    const publishArtistButton = artistId => {
        dispatch(publishArtist(artistId));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Top artists</Typography>
                </Grid>
                {user?.role === 'user' && (
                    <Grid item>
                        <Button color="primary" component={Link} to="/artists/new">Add artist</Button>
                    </Grid>
                )}
            </Grid>

            <Grid item container spacing={1}>
                {user?.role === 'admin' && (
                    <>
                        {loading ? (
                            <Grid container justify="center" alignItems="center" className={classes.progress}>
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : artists.map(artist => (
                            <ArtistItem
                                key={artist._id}
                                id={artist._id}
                                title={artist.title}
                                image={artist.image}
                                description={artist.description}
                                published={artist.published}
                                deleteButton={() => onDeleteHandler(artist._id)}
                                publishButton={() => publishArtistButton(artist._id)}
                            />
                        ))}
                    </>
                )}
                {user?.role === 'user' && (
                    <>
                        {loading ? (
                            <Grid container justify="center" alignItems="center" className={classes.progress}>
                                <Grid item>
                                    <CircularProgress />
                                </Grid>
                            </Grid>
                        ) : artists.map(artist => (
                            <React.StrictMode key={artist._id}>
                                {artist.published && (
                                    <ArtistItem
                                        key={artist._id}
                                        id={artist._id}
                                        title={artist.title}
                                        image={artist.image}
                                        description={artist.description}
                                        published={artist.published}
                                        deleteButton={() => onDeleteHandler(artist._id)}
                                        publishButton={() => publishArtistButton(artist._id)}
                                    />
                                )}
                            </React.StrictMode>
                        ))}
                    </>
                )}
                {user?.role === undefined && (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <Typography>Sign in to watch artists</Typography>
                        </Grid>
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
};

export default Artists;