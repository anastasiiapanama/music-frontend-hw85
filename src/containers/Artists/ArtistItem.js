import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Card, CardActionArea, CardActions, CardContent, CardMedia, Icon, IconButton} from "@material-ui/core";
import {apiURL} from "../../config";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import DeleteIcon from '@material-ui/icons/Delete';
import CloudOffIcon from '@material-ui/icons/CloudOff';
import CloudDoneIcon from '@material-ui/icons/CloudDone';

import imageNotAvailable from '../../assets/images/imageNotAvailable.png';
import {useSelector} from "react-redux";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 250,
    },
});

const ArtistItem = ({title, image, description, id, deleteButton, published, publishButton}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    if (published) {
        published = <Icon>
            <CloudDoneIcon/>
        </Icon>
    } else {
        published = <IconButton onClick={publishButton}>
            <CloudOffIcon/>
        </IconButton>
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.root}>
                <CardActionArea>
                    <CardMedia
                        image={cardImage}
                        title={title}
                        className={classes.media}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <IconButton component={Link} to={'/albums/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                    {user?.role === 'admin' && (
                        <IconButton onClick={deleteButton}>
                            <DeleteIcon />
                        </IconButton>
                    )}
                    {published}
                </CardActions>
            </Card>
        </Grid>
    );
};

ArtistItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    description: PropTypes.string
};

export default ArtistItem;