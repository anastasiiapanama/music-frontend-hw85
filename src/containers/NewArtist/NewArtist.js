import React from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ArtistForm from "../../components/ArtistForm/ArtistForm";
import {createArtist} from "../../store/actions/artistsActions";

const NewArtist = () => {
    const dispatch = useDispatch();

    const onArtistFormSubmit = async artistData => {
        dispatch(createArtist(artistData));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">Add new artist</Typography>
            </Grid>
            <Grid item xs>
                <ArtistForm
                    onSubmit={onArtistFormSubmit}
                />
            </Grid>
        </Grid>
    );
};

export default NewArtist;