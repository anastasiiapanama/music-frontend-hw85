import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {fetchTracks, publishTrack} from "../../store/actions/tracksActions";
import TrackItem from "./TrackItem";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import {fetchTrackHistory} from "../../store/actions/trackHistoryActions";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Tracks = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const tracks = useSelector(state => state.tracks.tracks);
    const loading = useSelector(state => state.tracks.tracksLoading);

    useEffect(() => {
        dispatch(fetchTracks(match.params.id));
    }, [dispatch, match.params.id]);

    const onPlaySendButton = (trackId) => {
        dispatch(fetchTrackHistory(trackId));
    };

    const publishTrackButton = id => {
        dispatch(publishTrack(id));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Top tracks</Typography>
                </Grid>
                {user?.role === 'user' && (
                    <Grid item>
                        <Button color="primary" component={Link} to="/tracks/new">Add track</Button>
                    </Grid>
                )}
            </Grid>
            <Grid item container direction="column" spacing={1}>
                {user?.role === 'admin' && (
                    <>
                        {loading ? (
                            <Grid container justify="center" alignItems="center" className={classes.progress}>
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : tracks.map(track => (
                            <TrackItem
                                key={track._id}
                                id={track._id}
                                trackNumber={track.trackNumber}
                                title={track.title}
                                duration={track.duration}
                                onPlayButton={() => onPlaySendButton(track._id)}
                                published={track.published}
                                publishButton={() => publishTrackButton(track._id)}
                            />
                        ))}
                    </>
                )}
                {user?.role === 'user' && (
                    <>
                        {loading ? (
                            <Grid container justify="center" alignItems="center" className={classes.progress}>
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : tracks.map(track => (
                            <React.StrictMode key={track._id}>
                                {track.published ? (
                                    <TrackItem
                                        id={track._id}
                                        trackNumber={track.trackNumber}
                                        title={track.title}
                                        duration={track.duration}
                                        onPlayButton={() => onPlaySendButton(track._id)}
                                        published={track.published}
                                    />
                                ) : (
                                    <Grid item>
                                        <Typography>No publish tracks yet</Typography>
                                    </Grid>
                                )}
                            </React.StrictMode>
                        ))}
                    </>
                )}

                {user?.role === undefined && (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <Typography>Sign in to watch tracks</Typography>
                        </Grid>
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
};

export default Tracks;