import React from 'react';
import PropTypes from 'prop-types';

import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Card, CardActions, CardContent, CardHeader, Icon, IconButton} from "@material-ui/core";
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import CloudDoneIcon from "@material-ui/icons/CloudDone";
import CloudOffIcon from "@material-ui/icons/CloudOff";
import DeleteIcon from "@material-ui/icons/Delete";
import {useSelector} from "react-redux";

const useStyles = makeStyles({
    card: {
        height: '100%',
        width: '70%',
        display: "flex",
        justifyContent: 'space-between'
    },
    content: {
        display: 'flex',
        alignItems: 'center'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});

const TrackItem = ({trackNumber, title, duration, onPlayButton, published, onDeleteTrackButton, publishButton}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    if (published) {
        published = <Icon>
            <CloudDoneIcon/>
        </Icon>
    } else {
        published = <IconButton onClick={publishButton}>
            <CloudOffIcon/>
        </IconButton>
    }

    return (
        <Grid item>
            <Card className={classes.card}>
                <CardHeader title={trackNumber}/>
                <CardContent className={classes.content}>
                    <strong style={{marginLeft: '10px'}}>
                        {title}
                    </strong>
                    <p style={{paddingLeft: '200px'}}>{duration} min</p>
                </CardContent>
                <CardActions>
                    <IconButton color="primary" onClick={onPlayButton}>
                        <PlayCircleOutlineIcon/>
                    </IconButton>
                    {user?.role === 'admin' && (
                        <>
                            <IconButton onClick={onDeleteTrackButton}>
                                <DeleteIcon/>
                            </IconButton>
                            {published}
                        </>
                    )}
                </CardActions>
            </Card>
        </Grid>
    );
};

TrackItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    duration: PropTypes.number
};

export default TrackItem;