import React from 'react';
import ReactDOM from 'react-dom';
import store from "./store/configureStore";
import {Provider} from 'react-redux';
import {Router} from 'react-router-dom';
import history from "./history";

import App from './App';

import {createMuiTheme, MuiThemeProvider} from "@material-ui/core";
import 'react-notifications/lib/notifications.css';
import {NotificationContainer} from "react-notifications";

const theme = createMuiTheme({
    props: {
        MuiTextField: {
            variant: 'outlined',
            fullWidth: true
        }
    }
});

const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <NotificationContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
