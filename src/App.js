import React from 'react';
import {Switch, Route} from "react-router-dom";

import Artists from "./containers/Artists/Artists";
import Tracks from "./containers/Tracks/Tracks";
import Albums from "./containers/Albums/Albums";

import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import NewArtist from "./containers/NewArtist/NewArtist";
import NewAlbum from "./containers/NewAlbum/NewAlbum";
import NewTrack from "./containers/NewTrack/NewTrack";

const App = () => (
    <>
        <CssBaseline/>
        <header>
            <AppToolbar/>
        </header>
        <main>
            <Container maxWidth="xl">
                <Switch>
                    <Route path="/" exact component={Artists}/>
                    <Route path="/artists/new" exact component={NewArtist}/>
                    <Route path="/tracks-new" exact component={NewTrack}/>
                    <Route path="/tracks/:id" exact component={Tracks}/>
                    <Route path="/albums-new" exact component={NewAlbum}/>
                    <Route path="/albums/:id" exact component={Albums}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/track_history" exact component={TrackHistory}/>
                </Switch>
            </Container>
        </main>
    </>
);

export default App;
