import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import artistsReducer from "./reducers/artistsReducer";
import tracksReducer from "./reducers/tracksReducer";
import albumsReducer from "./reducers/albumsReducer";
import usersReducer, {initialState} from "./reducers/usersReducer";
import trackHistoryReducer from "./reducers/trackHistoryReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";

const rootReducer = combineReducers({
    artists: artistsReducer,
    tracks: tracksReducer,
    albums: albumsReducer,
    users: usersReducer,
    trackHistory: trackHistoryReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            ...initialState,
            user: store.getState().users.user
        }
    });
});

export default store;
