import axiosApi from "../../axios-api";
import {historyPush} from "./historyaActions";
import {NotificationManager} from "react-notifications";

export const FETCH_ALBUMS_LIST_REQUEST = 'FETCH_ALBUMS_LIST_REQUEST';
export const FETCH_ALBUMS_LIST_SUCCESS = 'FETCH_ALBUMS_LIST_SUCCESS';
export const FETCH_ALBUMS_LIST_FAILURE = 'FETCH_ALBUMS_LIST_FAILURE';

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const CREATE_ALBUM_REQUEST = 'CREATE_ALBUM_REQUEST';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_FAILURE = 'CREATE_ALBUM_FAILURE';

export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';

export const PUBLISH_ALBUM_REQUEST = 'PUBLISH_ALBUM_REQUEST';
export const PUBLISH_ALBUM_SUCCESS = 'PUBLISH_ALBUM_SUCCESS';
export const PUBLISH_ALBUM_FAILURE = 'PUBLISH_ALBUM_FAILURE';

export const fetchAlbumsListRequest = () => ({type: FETCH_ALBUMS_LIST_REQUEST});
export const fetchAlbumsListSuccess = albums => ({type: FETCH_ALBUMS_LIST_SUCCESS, albums});
export const fetchAlbumsListFailure = () => ({type: FETCH_ALBUMS_LIST_FAILURE});

export const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});
export const fetchAlbumFailure = () => ({type: FETCH_ALBUMS_FAILURE});

export const createAlbumRequest = () => ({type: CREATE_ALBUM_REQUEST});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumFailure = error => ({type: CREATE_ALBUM_FAILURE, error});

export const deleteAlbumSuccess =  id => ({type: DELETE_ALBUM_SUCCESS, id});

export const publishAlbumRequest = () => ({type: PUBLISH_ALBUM_REQUEST});
export const publishAlbumSuccess = id => ({type: PUBLISH_ALBUM_SUCCESS, id});
export const publishAlbumFailure = () => ({type: PUBLISH_ALBUM_FAILURE});

export const fetchAlbumsList = () => {
    return async dispatch => {
        try {
            dispatch(fetchAlbumsListRequest());
            const response = await axiosApi.get('/albums');
            dispatch(fetchAlbumsListSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumsListFailure);
        }
    };
};

export const fetchAlbums = id => {
    return async dispatch => {
        try {
            dispatch(fetchAlbumsRequest());
            const response = await axiosApi.get('/albums?artist=' + id);
            dispatch(fetchAlbumsSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumFailure());
        }
    };
};

export const createAlbum = albumData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(createAlbumRequest());
            const response = await axiosApi.post('/albums', albumData, {headers});
            console.log(response)
            dispatch(createAlbumSuccess());
            dispatch(historyPush('/'));

            NotificationManager.success('Album created successfully');
        } catch (e) {
            dispatch(createAlbumFailure(e.response.data));
            NotificationManager.error('Could not create new album');
        }
    };
};

export const deleteAlbum = id => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            await axiosApi.delete('/albums/' + id, {headers});

            dispatch(deleteAlbumSuccess(id));
            dispatch(historyPush('/'));
        } catch (e) {
            NotificationManager.error('Could not delete album');
        }
    };
};

export const publishAlbum = (id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(publishAlbumRequest())
            await axiosApi.post('/albums/' + id,'',{headers});
            dispatch(publishAlbumSuccess());
            dispatch(fetchAlbumsList());
            NotificationManager.success('Album publish successfully');
        } catch (e) {
            dispatch(publishAlbumFailure());
        }
    };
};