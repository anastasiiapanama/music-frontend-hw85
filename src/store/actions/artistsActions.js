import axiosApi from "../../axios-api";
import {historyPush} from "./historyaActions";
import {NotificationManager} from "react-notifications";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';

export const CREATE_ARTIST_REQUEST = 'CREATE_ARTIST_REQUEST';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const CREATE_ARTIST_FAILURE = 'CREATE_ARTIST_FAILURE';

export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';

export const PUBLISH_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const PUBLISH_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const PUBLISH_ARTIST_FAILURE = 'DELETE_ARTIST_FAILURE';

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, artists});
export const fetchArtistsFailure = () => ({type: FETCH_ARTISTS_FAILURE});

export const createArtistRequest = () => ({type: CREATE_ARTIST_REQUEST});
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});
export const createArtistFailure = () => ({type: CREATE_ARTIST_FAILURE});

export const deleteArtistSuccess =  id => ({type: DELETE_ARTIST_SUCCESS, id});

export const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
export const publishArtistSuccess = artist => ({type: PUBLISH_ARTIST_SUCCESS, artist});
export const publishArtistFailure = () => ({type: PUBLISH_ARTIST_FAILURE});

export const fetchArtists = () => {
    return async dispatch => {
        try {
            dispatch(fetchArtistsRequest());

            const response = await axiosApi.get('/artists');
            dispatch(fetchArtistsSuccess(response.data));
        } catch (e) {
            dispatch(fetchArtistsFailure());
        }
    };
};

export const createArtist = artistData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(createArtistRequest());
            await axiosApi.post('/artists', artistData, {headers});
            dispatch(createArtistSuccess());
            dispatch(historyPush('/'));

            NotificationManager.success('Artist created successfully');
        } catch (e) {
            dispatch(createArtistFailure());
            NotificationManager.error('Could not create artist');
        }
    };
};

export const deleteArtist = id => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Authorization': token};

        await axiosApi.delete('/artists/' + id, {headers});
        dispatch(deleteArtistSuccess(id));
    };
};

export const publishArtist = id => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(publishArtistRequest())
            const response = await axiosApi.post('/artists/' + id,'',{headers});
            dispatch(publishArtistSuccess(response.data));
            dispatch(fetchArtists());

            NotificationManager.success('Artist publish successfully');
        } catch (e) {
            dispatch(publishArtistFailure());
        }
    };
};
