import axiosApi from "../../axios-api";
import {historyPush} from "./historyaActions";
import {NotificationManager} from "react-notifications";

export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';

export const CREATE_TRACK_REQUEST = 'CREATE_TRACK_REQUEST';
export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';
export const CREATE_TRACK_FAILURE = 'CREATE_TRACK_FAILURE';

export const PUBLISH_TRACK_REQUEST = 'PUBLISH_TRACK_REQUEST';
export const PUBLISH_TRACK_SUCCESS = 'PUBLISH_TRACK_SUCCESS';
export const PUBLISH_TRACK_FAILURE = 'PUBLISH_TRACK_FAILURE';

export const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});
export const fetchTracksFailure = () => ({type: FETCH_TRACKS_FAILURE});

export const createTrackRequest = () => ({type: CREATE_TRACK_REQUEST});
export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackFailure = error => ({type: CREATE_TRACK_FAILURE, error});

export const publishTrackRequest = () => ({type: PUBLISH_TRACK_REQUEST});
export const publishTrackSuccess = () => ({type: PUBLISH_TRACK_SUCCESS});
export const publishTrackFailure = () => ({type: PUBLISH_TRACK_FAILURE});

export const fetchTracks = id => {
    return async dispatch => {
        try {
            dispatch(fetchTracksRequest());

            const response = await axiosApi.get('tracks?album=' + id);
            dispatch(fetchTracksSuccess(response.data));
        } catch (e) {
            dispatch(fetchTracksFailure());
        }
    };
};

export const createTrack = trackData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(createTrackRequest());
            const response = await axiosApi.post('/tracks', trackData, {headers});
            console.log(response)
            dispatch(createTrackSuccess(response.data));
            dispatch(historyPush('/'));

            NotificationManager.success('Track created successfully');
        } catch (e) {
            dispatch(createTrackFailure(e.response.data));
            NotificationManager.error('Could not create new track');
        }
    };
};

export const publishTrack = id => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            dispatch(publishTrackRequest());
            await axiosApi.post('/tracks/' + id, '', {headers});
            dispatch(publishTrackSuccess());
            NotificationManager.success('Track publish successfully');
        } catch (e) {
            dispatch(publishTrackFailure());
        }
    }
}
