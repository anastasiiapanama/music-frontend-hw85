import axiosApi from "../../axios-api";

export const TRACK_HISTORY_REQUEST = 'TRACK_HISTORY_REQUEST';
export const TRACK_HISTORY_SUCCESS = 'TRACK_HISTORY_SUCCESS';
export const TRACK_HISTORY_FAILURE = 'TRACK_HISTORY_FAILURE';

export const HISTORY_LIST_REQUEST = 'HISTORY_LIST_REQUEST';
export const HISTORY_LIST_SUCCESS = 'HISTORY_LIST_SUCCESS';
export const HISTORY_LIST_FAILURE = 'HISTORY_LIST_FAILURE';

const trackHistoryRequest = () => ({type: TRACK_HISTORY_REQUEST});
const trackHistorySuccess = track => ({type: TRACK_HISTORY_SUCCESS, track});
const trackHistoryFailure = () => ({type: TRACK_HISTORY_FAILURE});

const historyListRequest = () => ({type: HISTORY_LIST_REQUEST});
const historyListSuccess = tracks => ({type: HISTORY_LIST_SUCCESS, tracks});
const historyListFailure = () => ({type: HISTORY_LIST_FAILURE});

export const historyList = id => {
    return async (dispatch, getState) => {
        try {
            dispatch(historyListRequest());
            const token = getState().users.user.token;
            const headers = {Authorization: token}
            const response = await axiosApi.get('/track_history?user=' + id, {headers});
            dispatch(historyListSuccess(response.data));
        } catch (e) {
            dispatch(historyListFailure());
        }
    };
};

export const fetchTrackHistory = (id) => {
    return async (dispatch, getState) => {
        try {
            dispatch(trackHistoryRequest());
            const token = getState().users.user.token;
            const headers = {Authorization: token}
            const response = await axiosApi.post('/track_history', {track: id}, {headers});
            dispatch(trackHistorySuccess(response.data));
            dispatch(historyListSuccess());
        } catch (e) {
            dispatch(trackHistoryFailure());
        }
    };
};