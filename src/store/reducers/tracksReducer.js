import {
    CREATE_TRACK_FAILURE,
    CREATE_TRACK_REQUEST,
    CREATE_TRACK_SUCCESS,
    FETCH_ALL_TRACKS_FAILURE,
    FETCH_ALL_TRACKS_REQUEST,
    FETCH_ALL_TRACKS_SUCCESS,
    FETCH_TRACKS_FAILURE,
    FETCH_TRACKS_REQUEST,
    FETCH_TRACKS_SUCCESS
} from "../actions/tracksActions";

const initialState = {
    tracks: [],
    track: {},
    tracksLoading: false,
    createTrackError: false,
    createTrackLoading: false
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_REQUEST:
            return {...state, tracksLoading: true};
        case FETCH_TRACKS_SUCCESS:
            return {...state, tracksLoading: false, tracks: action.tracks};
        case FETCH_TRACKS_FAILURE:
            return {...state, tracksLoading: false};
        case CREATE_TRACK_REQUEST:
            return {...state, createTrackLoading: true};
        case CREATE_TRACK_SUCCESS:
            return {...state, createTrackLoading: false};
        case CREATE_TRACK_FAILURE:
            return {...state, createTrackLoading: false, createTrackError: action.error};
        default:
            return state;
    }
};

export default tracksReducer;