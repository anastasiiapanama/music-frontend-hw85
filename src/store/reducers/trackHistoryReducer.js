import {
    HISTORY_LIST_FAILURE,
    HISTORY_LIST_REQUEST, HISTORY_LIST_SUCCESS,
    TRACK_HISTORY_FAILURE,
    TRACK_HISTORY_REQUEST,
    TRACK_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
    track: {},
    tracks: [],
    trackLoading: false
};

const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case TRACK_HISTORY_REQUEST:
            return {...state, trackLoading: true};
        case TRACK_HISTORY_SUCCESS:
            return {...state, trackLoading: false, track: action.track};
        case TRACK_HISTORY_FAILURE:
            return {...state, trackLoading: false};
        case HISTORY_LIST_REQUEST:
            return {...state, trackLoading: true};
        case HISTORY_LIST_SUCCESS:
            return {...state, trackLoading: false, tracks: action.tracks};
        case HISTORY_LIST_FAILURE:
            return {...state, trackLoading: false};
        default:
            return state;
    };
};

export default trackHistoryReducer;