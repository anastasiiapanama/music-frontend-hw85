import {
    CREATE_ALBUM_FAILURE,
    CREATE_ALBUM_REQUEST, CREATE_ALBUM_SUCCESS, DELETE_ALBUM_SUCCESS,
    FETCH_ALBUMS_FAILURE, FETCH_ALBUMS_LIST_FAILURE, FETCH_ALBUMS_LIST_REQUEST, FETCH_ALBUMS_LIST_SUCCESS,
    FETCH_ALBUMS_REQUEST,
    FETCH_ALBUMS_SUCCESS, PUBLISH_ALBUM_FAILURE, PUBLISH_ALBUM_REQUEST, PUBLISH_ALBUM_SUCCESS
} from "../actions/albumsActions";

const initialState = {
    albums: [],
    albumsListLoading: false,
    albumsLoading: false,
    createAlbumLoading: false,
    createAlbumError: false,
    publishAlbumLoading: false,

};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_LIST_REQUEST:
            return {...state, albumsListLoading: true};
        case FETCH_ALBUMS_LIST_SUCCESS:
            return {...state, albumsListLoading: false, albums: action.albums};
        case FETCH_ALBUMS_LIST_FAILURE:
            return {...state, albumsListLoading: false};
        case FETCH_ALBUMS_REQUEST:
            return {...state, albumsLoading: true};
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albumsLoading: false, albums: action.albums};
        case FETCH_ALBUMS_FAILURE:
            return {...state, albumsLoading: false};
        case CREATE_ALBUM_REQUEST:
            return {...state, createAlbumLoading: true};
        case CREATE_ALBUM_SUCCESS:
            return {...state, createAlbumLoading: false};
        case CREATE_ALBUM_FAILURE:
            return {...state, createAlbumLoading: false, createAlbumError: action.error};
        case DELETE_ALBUM_SUCCESS:
            return {
                ...state,
                albums: state.albums.filter(c => c.id !== action.id)
            };
        case PUBLISH_ALBUM_REQUEST:
            return {...state, publishAlbumLoading: true};
        case PUBLISH_ALBUM_SUCCESS:
            return {...state, publishAlbumLoading: false, albums: action.id};
        case PUBLISH_ALBUM_FAILURE:
            return {...state, publishAlbumLoading: false}
        default:
            return state;
    }
};

export default albumsReducer;