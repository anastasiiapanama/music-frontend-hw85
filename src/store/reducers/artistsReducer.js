import {
    CREATE_ARTIST_FAILURE,
    CREATE_ARTIST_REQUEST, CREATE_ARTIST_SUCCESS, DELETE_ARTIST_SUCCESS,
    FETCH_ARTISTS_FAILURE,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS, PUBLISH_ARTIST_FAILURE, PUBLISH_ARTIST_REQUEST, PUBLISH_ARTIST_SUCCESS
} from "../actions/artistsActions";


const initialState = {
    artists: [],
    artist: {},
    artistsLoading: false,
    publishArtistLoading: false
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_REQUEST:
            return {...state, artistsLoading: true};
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artistsLoading: false, artists: action.artists};
        case FETCH_ARTISTS_FAILURE:
            return {...state, artistsLoading: false};
        case CREATE_ARTIST_REQUEST:
            return {...state, createArtistLoading: true};
        case CREATE_ARTIST_SUCCESS:
            return {...state, createArtistLoading: false};
        case CREATE_ARTIST_FAILURE:
            return {...state, createArtistLoading: false};
        case DELETE_ARTIST_SUCCESS:
            return {
                ...state,
                artists: state.artists.filter(c => c.id !== action.id)
            };
        case PUBLISH_ARTIST_REQUEST:
            return {...state, publishArtistLoading: true};
        case PUBLISH_ARTIST_SUCCESS:
            return {...state, publishArtistLoading: false, artist: action.artist};
        case PUBLISH_ARTIST_FAILURE:
            return {...state, publishArtistLoading: false}
        default:
            return state;
    }
};

export default artistsReducer;